const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const Quiz = mongoose.model('Quiz');

router.get('/', (req, res) => {
    res.render("quiz/addOrEdit", {
        viewTitle: "Insert Quiz Questions"
    });
});

router.post('/', (req, res) => {
    if (req.body._id == '')
        insertRecord(req, res);
        else
        updateRecord(req, res);
});


function insertRecord(req, res) {
    var quiz = new Quiz();
    quiz.question = req.body.question;
    quiz.option01 = req.body.option01;
    quiz.option02 = req.body.option02;
    quiz.option03 = req.body.option03;
    quiz.option04 = req.body.option04;
    quiz.answer = req.body.answer;
    quiz.save((err, doc) => {
        if (!err)
            res.redirect('quiz/list');
        else {
            if (err.name == 'ValidationError') {
                handleValidationError(err, req.body);
                res.render("quiz/addOrEdit", {
                    viewTitle: "Insert Quiz Questions",
                    quiz: req.body
                });
            }
            else
                console.log('Error during record insertion : ' + err);
        }
    });
}

function updateRecord(req, res) {
    Quiz.findOneAndUpdate({ _id: req.body._id }, req.body, { new: true }, (err, doc) => {
        if (!err) { res.redirect('quiz/list'); }
        else {
            if (err.name == 'ValidationError') {
                handleValidationError(err, req.body);
                res.render("quiz/addOrEdit", {
                    viewTitle: 'Update Quiz',
                    quiz: req.body
                });
            }
            else
                console.log('Error during record update : ' + err);
        }
    });
}


router.get('/list', (req, res) => {
    Quiz.find((err, docs) => {
        if (!err) {
            res.render("quiz/list", {
                list: docs
            });
        }
        else {
            console.log('Error in retrieving quiz list :' + err);
        }
    });
});


function handleValidationError(err, body) {
    for (field in err.errors) {
        switch (err.errors[field].path) {
            case 'question':
                body['questionError'] = err.errors[field].message;
                break;
            case 'option01':
                body['errorOption01'] = err.errors[field].message;
                break;

                case 'option02':
                    body['errorOption02'] = err.errors[field].message;
                    break;

                    case 'option03':
                        body['errorOption03'] = err.errors[field].message;
                        break;
                        case 'option04':
                            body['errorOption04'] = err.errors[field].message;
                            break;
                            case 'answer':
                                body['errorAnswer'] = err.errors[field].message;
                                break;
            default:
                break;
        }
    }
}

router.get('/:id', (req, res) => {
    Quiz.findById(req.params.id, (err, doc) => {
        if (!err) {
            res.render("quiz/addOrEdit", {
                viewTitle: "Update Quiz",
                quiz: doc
            });
        }
    });
});

router.get('/delete/:id', (req, res) => {
    Quiz.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) {
            res.redirect('/quiz/list');
        }
        else { console.log('Error in quiz delete :' + err); }
    });
});

module.exports = router;